exports.help = {
  main: (Client, prefix) => {
    let embed = {
      title: `**Arctic Studios Main Help Menu**`,
      description: `Welcome to \`Arctic-Discord-Bot 2.0\`, a singular part of the Arctic 2.0 Update to make our team even better.\n \nWe found ourselves to have a big issue where our team was growing at a faster rate than our bot. We needed something that is exactly like our team; Growing, expanding and improving.`,
      fields: [
        { name: `Miscellaneous`, value: `\`${prefix}help misc\`` },
        { name: `Tickets`, value: `\`${prefix}help tickets\`` },
        { name: `Commissions`, value: `\`${prefix}help commissions\`` },
        { name: `Staff`, value: `\`${prefix}help staff\`` },
        { name: `Economy`, value: `\`${prefix}help economy\`` },
        { name: `Games`, value: `\`${prefix}help games\`` },
        { name: `Store`, value: `\`${prefix}help store\`` },
        { name: `Payments`, value: `\`${prefix}help payments\`` },
        { name: `Payouts`, value: `\`${prefix}help payouts\`` },
        { name: `Referrals`, value: `\`${prefix}help referrals\`` },
        /* { name: `Partners`, value: `\`${prefix}help partners\`` }, */
        /* { name: `Advertisements`, value: `\`${prefix}help ads\`` }, */
        { name: `Panel`, value: `\`${prefix}help panel\`` }
      ]
    }
    return embed;
  },
  misc: (Client, prefix) => {
    let embed = {
      title: `**Arctic Miscelaneous Help Menu**`,
      description: `Below is a list of our Miscellaneous commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `Get a Help Menu`, value: `\`${prefix}help\`` },
        { name: `View Arctic Stats`, value: `\`${prefix}stats\`` },
        { name: `Make a Suggestion`, value: `\`${prefix}suggest <suggestion>\`` },
        { name: `Report a Bug`, value: `\`${prefix}bug <bug description>\`` },
      ]
    }
    return embed;
  },
  tickets: (Client, prefix) => {
    let embed = {
      title: `**Arctic Tickets Help Menu**`,
      description: `Below is a list of our Ticket related commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `Open a Ticket`, value: `\`${prefix}new [reason]\`` },
        { name: `Open an HR Ticket`, value: `\`${prefix}hr [reason]\`` },
        { name: `Close a Ticket`, value: `\`${prefix}close [reason]\`` },
        { name: `Add a User`, value: `\`${prefix}add <username>\`` },
        { name: `Remove a User`, value: `\`${prefix}remove <username>\`` },
        { name: `Toggle Alerts`, value: `\`${prefix}alerts <true/false>\`` },
        { name: `Open a Voice Channel`, value: `\`${prefix}voice new\``},
        { name: `Close a Voice Channel`, value: `\`${prefix}voice close\`` },
      ]
    }
    return embed;
  },
  commissions: (Client, prefix) => {
    let embed = {
      title: `**Arctic Commissions Help Menu**`,
      description: `Below is a list of our Commission related commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `Create a Commission`, value: `\`${prefix}commission create\`` },
        { name: `Info on a Commission`, value: `\`${prefix}commission info\`` },
        { name: `Edit a Commission`, value: `\`${prefix}commission edit\`` },
        { name: `Repost a Commission`, value: `\`${prefix}commission repost\`` },
        { name: `Unclaim a Commission`, value: `\`${prefix}commission unclaim\`` },
        { name: `Delete a Commission`, value: `\`${prefix}commission delete\`` }
      ]
    }
    return embed;
  },
  games: (Client, prefix) => {
    let embed = {
      title: `**Arctic Games Help Menu**`,
      description: `Below is a list of our Game related commands. Games are experimental. If you find a bug, report it. If you have an idea for a game, let us know.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `Blackjack`, value: `\`${prefix}bj <stake>\`` },
        { name: `Slots`, value: `\`${prefix}slots <stake>\`` },
        { name: `Coinflip`, value: `\`${prefix}flip <heads/tails> <stake>\`` }
      ]
    }
    return embed;
  },
  staff: (Client, prefix) => {
    let embed = {
      title: `**Arctic Staff Help Menu**`,
      description: `Below is a list of our Staff related commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `View a Profile`, value: `\`${prefix}staff profile [user]\`` },
        { name: `View a Portfolio`, value: `\`${prefix}staff portfolio <user/type>\`` },
        { name: `Add a Staff Member`, value: `\`${prefix}staff add <user>\`` },
        { name: `Remove a Staff Member`, value: `\`${prefix}staff remove <user>\`` },
        { name: `Edit a Staff Member`, value: `\`${prefix}staff edit\`` },
        { name: `Post a Review`, value: `\`${prefix}review\``},
        { name: `List all Staff`, value: `\`${prefix}staff list\``}
      ]
    }
    return embed;
  },
  payments: (Client, prefix) => {
    let embed = {
      title: `**Arctic Payments Help Menu**`,
      description: `Below is a list of our Payment related commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `Create a Payment`, value: `\`${prefix}payment create\`` },
        { name: `Find a Payment`, value: `\`${prefix}payment find\`` },
        { name: `Delete a Payment`, value: `\`${prefix}payment delete\``}
      ]
    }
    return embed;
  },
  economy: (Client, prefix) => {
    let embed = {
      title: `**Arctic Economy Help Menu**`,
      description: `Below is a list of our Economy related commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `View a Profile`, value: `\`${prefix}profile [username]\`` },
        { name: `XP Leaderboard`, value: `\`${prefix}leaderboard\`` },
        { name: `Coins Leaderboard`, value: `\`${prefix}baltop\`` },
        { name: `Send Money`, value: `\`${prefix}pay <username> <amount>\`` }
      ]
    }
    return embed;
  },
  panel: (Client, prefix) => {
    let embed = {
      title: `**Arctic Panel Help Menu**`,
      description: `Below is a list of our Panel related commands. The panel is still in alpha testing, please report any bugs or issues that you find.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `Get the Panel Link`, value: `\`${prefix}panel\`` }
      ]
    }
    return embed;
  },
  referrals: (Client, prefix) => {
    let embed = {
      title: `**Arctic Referrals Help Menu**`,
      description: `Below is a list of our Referral related commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `Referrals Leaderboard`, value: `\`${prefix}leaderboard refs\`` },
        { name: `Total Referrals`, value: `\`${prefix}referrals [user]\``}
      ]
    }
    return embed;
  },
  partners: (Client, prefix) => {
    let embed = {
      title: `**Arctic Partners Help Menu**`,
      description: `Below is a list of our Partner related commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `Request a Partnership`, value: `\`${prefix}partner create\`` },
        { name: `Edit a Partnership`, value: `\`${prefix}partner edit\`` },
        { name: `Partner Info`, value: `\`${prefix}partner <username>\`` }
      ]
    }
    return embed;
  },
  store: (Client, prefix) => {
    let embed = {
      title: `**Arctic Store Help Menu**`,
      description: `Below is a list of our Store related commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `View the Store`, value: `\`${prefix}store\`` },
        { name: `More info on a store item`, value: `\`${prefix}store [itemID]\`` },
        { name: `Purchase an Item`, value: `\`${prefix}buy [itemID]\`` },
        { name: `View your Inventory`, value: `\`${prefix}inv\`` }
      ]
    }
    return embed;
  },
  ads: (Client, prefix) => {
    let embed = {
      title: `**Arctic Advertisments Help Menu**`,
      description: `Below is a list of our Advertisement related commands.\n \n<> => Required | [] => Optional`,
      fields: [

      ]
    }
    return embed;
  },
  payouts: (Client, prefix) => {
    let embed = {
      title: `**Arctic Payouts Help Menu**`,
      description: `Below is a list of our Payout related commands.\n \n<> => Required | [] => Optional`,
      fields: [
        { name: `Create a Payouts`, value: `\`${prefix}payouts create\`` },
        { name: `Find a Payouts`, value: `\`${prefix}payouts find\`` },
        { name: `Delete a Payouts`, value: `\`${prefix}payouts delete\``}
      ]
    }
    return embed;
  }
}

exports.misc = {
  suggest: {
    errors: {
      disabled: `**Disabled Feature** => Unfortunately, that feature has been disabled.`,
      invalidSyntax: `**Invalid Syntax** => Follow that command with a suggestion.`
    },
    response: `Thanks for your suggestion. It has been submitted for voting in #suggestions`,
    embed: (Client, msg, args) => {
      let embed = {
        title: `${msg.author.username}'s suggestion`,
        description: args.suggestion,
        thumbnail: msg.author.displayAvatarURL(),
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      }
      return embed;
    },
    completed: `**Thanks for your Suggestion** => It has been submitted for voting in #suggestions.`
  },
  bugs: {
    errors: {
      disabled: `**Feature Disabled** => That feature has been disabled. Please create the \`bugs\` text channel to enable this.`,
      invalidSyntax: `**Invalid Syntax** => You must include a bug with your report. (e.g. // -bug command doesn't work)`
    },
    embed: (Client, msg, args) => {
      let embed = {
        title: `${msg.author.username}'s Bug Report`,
        description: args.bug,
        thumbnail: msg.author.displayAvatarURL(),
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      }
      return embed;
    },
    completed: `**Thanks for your Bug Report** => It has been submitted for verification in #bugs.`
  },
  ping: {
    response: `Pong!`
  },
  pin: {
    errors: {
      invalidRole: `**Invalid Permissions** => You lack permissions to execute that command.`,
      invalidChannel: `**Invalid Permissions** => That command can only be executed in a ticket channel.`,
      invalidParmas: `**Invalid Syntax** => You must follow that message with the ID of the message you would like to pin. (e.g. // -pin 462060505113034753)`,
      invalidID: `**Invalid Syntax** => The ID provided was not found in this channel.`
    },
    response: `Message Pinned`
  },
  stats: {
    response: `Stats Command`,
    embed: (Client, msg, params, args) => {
      let embed = {
        title: `__**Arctic Studios Stats**__`,
        description: ``,
        fields: [
          { name: `Channels`, value: `${Client.client.channels.size.toLocaleString()}` },
          { name: `Users`, value: `${args.totalUserCount}` },
          { name: `Tickets (open/closed)`, value: `${args.tickets.countToUse} (${args.tickets.activeCount} / ${args.tickets.inactiveCount})` },
          { name: `Total Coins`, value: `${args.totalBank}` },
          { name: `Invoices`, value: `${args.invoice}` },
          { name: `Memory Usage`, value: `${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB` },
          { name: `Application Uptime`, value: (Math.round(Client.client.uptime / (1000 * 60 * 60))) + " Hours, " + (Math.round(Client.client.uptime / (1000 * 60)) % 60) + " minutes, and " + (Math.round(Client.client.uptime / 1000) % 60) + " seconds." },
          { name: `Guild Created`, value: `${msg.guild.createdAt}` }
        ],
        thumbnail: Client.client.user.avatarURL()
      };
      return embed;
    },
    message: (Client, msg, args) => {
      let embed = {
        title: `Arctic Studios Statistics`,
        fields: [
          { name: `Total Economy`, value: `$${args.totalEco}` },
          { name: `NodeJS Version`, value: process.version },
          { name: `Discord.JS Version`, value: Client.modules.Discord.version },
          { name: `Application Uptime`, value: (Math.round(Client.client.uptime / (1000 * 60 * 60))) + " Hours, " + (Math.round(Client.client.uptime / (1000 * 60)) % 60) + " minutes, and " + (Math.round(Client.client.uptime / 1000) % 60) + " seconds." }
        ]
      };
      return embed;
    }
  },
  join: (Client, member, invite, inviter) => {
    let message = [
      `Howdy diddly`,
      `Well come on in`,
      `And on this rock I shall build Arctic`,
      `And on the third day god said. Let there be Arctic Studios.`,
      `Welcome to the theater of memes`,
      `Beam me up Scotty`,
      `Does anybody read these?`,
      `Beep boop`,
      `Watch live Arctic Studios Development @ https://twitch.tv/dectom99`,
      `Delivering high-quality products...`
      `We're going on a trip in our favorite rocketship.`
      `Welcome message here...`
    ]
    let embed = {
      title: `Welcome ${member.user.tag}`,
      description: `${message[Math.floor(Math.random() * message.length)]}\n \nInvited By: \`${inviter.tag}\` => ${invite.uses} Invites`,
      thumbnail: member.user.displayAvatarURL()
    };
    return embed;
  },
  joinDM: (Client, member) => {
    let embed = {
      title: `Welcome to Arctic Studios`,
      thumbnail: Client.client.user.displayAvatarURL(),
      description: `Thank you for joining Arctic Studios! If you are looking to apply, please open an HR ticket with \`-hr\` and let our team know.\n \nIf you are interested in ordering a product or just looking for a quote, create a ticket and let us know by using our easy to use, fully automated quotes and commissions systems. To get started do \`-new\`.\n \nBy default you will have ticket alerts turned on, this means that when you create a new ticket, every message sent within it (excluding from you) will ping you to respond. You can disable this at any time with \`-alerts false\` and re-enable with \`-alerts true\`.`
    };
    return embed;
  },
  messages: {
    errors: {
      spamDetected: `**Sending Too Fast** => Woah there, we detected that you are sending messages that are similar in a speedy manor. Please calm down and let others use the chat also.`
    }
  }
}

exports.moderation = {
  ban: {
    errors: {
      invalidPermissions: `**Invalid Permissions** => You must have the \`Human Resources\` role or higher to ban a user.`,
      noUserTagged: `**Invalid Syntax** => You must mention a user to ban.`,
      noReasonProvided: `**Invalid Syntax** => You must provide a reason for the ban.`,
      insufficientPermissions: `**Invalid Permissions** => You must have a higher role than the user that you are trying to ban.`,
      noPunishmentsChannel: `**Invalid Channel** => There is not a channel by the name of \`punishments\`.`
    },
    banned: (Client, user, reason, staff) => {
      let embed = {
        title: `A User has been Banned.`,
        fields: [
          { name: `Reason`, value: `${reason}`, inline: true },
          { name: `Banned User`, value: `${user.user.tag} (${user.id})` },
          { name: `Staff Member`, value: `${staff.tag} (${staff.id})` }
        ],
        thumbnail: user.user.displayAvatarURL(),
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      }
      return embed;
    },
    dm: (Client, user, reason, staff) => {
      let embed = {
        title: `You have been Banned.`,
        fields: [
          { name: `Reason`, value: `${reason}`, inline: true },
          { name: `Banned User`, value: `${user.user.tag} (${user.user.id})` },
          { name: `Staff Member`, value: `${staff.tag} (${staff.id})` },
          { name: `Unfair Punishment?`, value: `If you feel like this punishment was unfair and you would like it removed from your history, then DM an HR member.` }
        ],
        thumbnail: user.user.displayAvatarURL(),
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      }
      return embed;
    }
  },
  kick: {
    errors: {
      invalidPermissions: `**Invalid Permissions** => You must have the \`Public Relations\` role or higher to kick a user.`,
      noUserTagged: `**Invalid Syntax** => You must mention a user to kick.`,
      noReasonProvided: `**Invalid Syntax** => You must provide a reason for the kick.`,
      insufficientPermissions: `**Invalid Permissions** => You must have a higher role than the user that you are trying to kick.`,
      noPunishmentsChannel: `**Invalid Channel** => There is not a channel by the name of \`punishments\``
    },
    kicked: (Client, user, reason, staff) => {
      let embed = {
        title: `A User has been Kicked.`,
        fields: [
          { name: `Reason`, value: `${reason}`, inline: true },
          { name: `Kicked User`, value: `${user.user.tag} (${user.id})` },
          { name: `Staff Member`, value: `${staff.tag} (${staff.id})` }
        ],
        thumbnail: user.user.displayAvatarURL(),
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      }
      return embed;
    },
    dm: (Client, user, reason, staff) => {
      let embed = {
        title: `You have been Kicked.`,
        fields: [
          { name: `Reason`, value: `${reason}`, inline: true },
          { name: `Kicked User`, value: `${user.user.tag} (${user.user.id})` },
          { name: `Staff Member`, value: `${staff.tag} (${staff.id})` },
          { name: `Unfair Punishment?`, value: `If you feel like this punishment was unfair and you would like it removed from your history, open an HR ticket and discuss the issue with an HR member.` }
        ],
        thumbnail: user.user.displayAvatarURL(),
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      }
      return embed;
    }
  }
}

exports.tickets = {
  errors: {
    noArgs: `**Invalid Syntax** => You must reply to that command with either \`true\` or \`false\`.`,
    invalidReply: `**Invalid Syntax** => You provided an improper value.`,
    cancel: `**Canceling Tickets Process** => Exiting the dialogue...`,
    invalidSalesRep: `**Invalid Syntax** => The Arctic Rep specified wasn't found. We have assigned this ticket to all our Reps.`,
    noAlertsArgs: `**Invalid Syntax** => You must follow that command with a \`true\` or \`false\` value.`,
    invalidUserName: `**Invalid Syntax** => The user you entered was not found.`,
    invalidChannel: `**Invalid Syntax** => You must be in a \`ticket\` channel to execute that command.`,
    ticketNotFound: `**Invalid Permissions** => The ticket was not found in our database, please contact Dectom.`,
    voiceExists: `**Invalid Permissions** => There is already a voice channel for this ticket. Please use that.`,
    voiceDoesntExists: `**Invalid Permissions** => There is no voice channel associated with this ticket. You can open one with \`-voice new\`.`,
    invalidMovePerms: `**Invalid Permissions** => You must have the \`Arctic Representative\` role or higher to execute that command.`
  },
  alerts: {
    complete: (Client, value) => {
      let response = `**Updated Ticket Alerts** => Successfully switched ticket alerts to ${value}.`;
      return response;
    }
  },
  hr: {
    welcome: (Client, args) => {
      let embed = {
        title: `Arctic HR Ticket`,
        description: `Welcome \`${args.user.tag}\` to Arctic Studios HR Ticket.\n \nPlease select one of the reactions below to decide how you want to proceed.` +
        `\n:regional_indicator_a: => Speak to a Board of Directors Member.` +
        `\n:regional_indicator_b: => Speak to an HR Representative.` +
        //`\n:regional_indicator_c: => Apply for a position.` +
        `\n \nYou opened this ticket with the reason:\n\`\`\`${args.reason}\`\`\` `
      }
      return embed;
    },
    option: (Client, args, item) => {
      let description = `You chose option ${item}.`
      let extra = ``;
      if (item === "🇦") extra = `Our Board of Directors Team has been alerted to this ticket and will reply shortly.`
      if (item === "🇧") extra = `Our Human Resources Team has been alerted to this ticket and will reply shortly.`
      //if (item === "🇨") extra = `\n \nWelcome to Arctic's Application Process. We will ask you a few questions about your application then pass it off to our Human Resources Team.\n \nBe Aware. Continuing with this applicaiton you agree to our [TOS](https://arcticstudios.org) and agree that you may be tested in your field and required to create something for us.\n \nWant to Quit ? Just type \`cancel\` at any time or wait 250 secconds.\n \nPlease reply with the name of the position you would like to apply for.`
      let embed = {
        title: `Arctic HR Ticket => ${item}`,
        description: `${extra} `
      };
      return embed;
    },
    created: (Client, args, cnl) => {
      let embed = {
        title: `New Arctic HR Ticket`,
        description: `Thanks \`${args.user.tag}\` for creating an HR Ticket. You can find it here ${cnl}`
      }
      return embed;
    },
    application: {
      age: `**Arctic HR Application**\n \nPlease reply with your current age.`,
      portfolio: `**Arctic HR Application\n \nPlease reply with your current portfolio. (**DO NOT** upload any attatchments. Only links.)`,
      confirm: (Client, args) => {
        let embed = {
          title: ``
        };
        return embed;
      }
    }
  },
  normal: {
    created: (Client, args, cnl) => {
      let embed = {
        title: `New Arctic Ticket`,
        description: `Thanks \`${args.user.tag}\` for creating a Ticket. You can find it here ${cnl}`
      }
      return embed;
    },
    welcome: (Client, args) => {
      let embed = {
        title: `Arctic Tickets System`,
        description: `Welcome \`${args.user.tag}\` to Arctic Studios.\n \nPlease select one of the reactions below to decide how you want to proceed.\n ` +
          `\n:regional_indicator_a: => Get a random Sales Rep.` +
          `\n:regional_indicator_b: => Assign a specific Sales Rep.` +
          //`\n:regional_indicator_c: => Automatically Post a Quote` +
          `\n \nYou opened this ticket with the reason:\n\`\`\`${args.reason}\`\`\` `
      }
      return embed;
    },
    option: (Client, args, item) => {
      let description = `You chose option ${item}.`
      let extra = ``;
      if (item === "🇦") extra = `Our team of Arctic Representatives have been alerted and someone will be with you soon!`;
      if (item === "🇧") extra = `Please reply with the name of the Sales Representative that you would like to handle your ticket.`;
      //if (item === "🇨") extra = `Welcome to Arctic's Quotes System, in this dialogue we will ask you questions for details to post to our freelancers for the most active quote.\n \nWant to Quit? Just type \`cancel\` at any time or wait 100 secconds.\n \nPlease enter the name of the roles your project applies to.`
      let embed = {
        title: `Arctic HR Ticket => ${item}`,
        description: `${extra} `
      };
      return embed;
    },
    selectedAR: (Client, staff) => {
      let embed = {
        title: `**Arctic Representative Assigned**`,
        description: `We have assigned \`${staff.staffName}\` to you ticket as per your request.`
      };
      return embed;
    }
  },
  close: {
    errors: {
      invalidGuid: "**Invalid Channel** => That command can only be executed on a server!",
      noTicketsCat: "**Tickets Disabled** => Tickets are Disabled on this Server. To enable create a category named `Tickets`.",
      invalidChannel: "**Invalid Channel** => That command may only be executed in a channel labeled `ticket`.",
      invalidReason: "**Inalid Syntax** => You must provide a reason to close a ticket channel."
    },
    response: `Channel Closing`,
    embed: (Client, msg, args) => {
      let embed = {
        title: `Arctic Studios Tickets System`,
        description: "Thanks for contacting Arctic Studios. If you need further support please don't hesitate to open another ticket."
      }
      return embed;
    },
    confirmation: (Client, args) => {
      let embed = {
        title: `Arctic Studios Ticket Closing`,
        description: `By reacting with :white_check_mark: you will close this ticket` +
          `\n \nReason Provided:\`\`\`${args.reason}\`\`\` `
      };
      return embed;
    }
  },
  add: {
    success: (Client, msg, args, result) => {
      let embed = {
        title: `Added ${result.userName} to the Ticket`,
        thumbnail: result.userProfileLink
      }
      return embed;
    }
  },
  remove: {
    success: (Client, msg, args, result) => {
      let embed = {
        title: `Removed ${result.userName} from the Ticket`,
        thumbnail: result.userProfileLink
      }
      return embed;
    }
  },
  voice: {
    success: `**Voice Channel Created**\n \nSuccessfully created a private voice channel for you to discuss your project in. If you are done with it please run the command \`-voice close\` to remove the channel.`,
    close: `**Voice Channel Deleted**\n \nSuccessfully closed the private voice channel. If you need another voice channel please feel free to re-open it with \`-voice new\`.`
  },
  updated: (Client, args, cnl) => {
    let embed = {
      title: `Updated Channel Category`,
      description: `Ticket has been updated and moved to the \`${cnl.type}\` Category`,
      thumbnail: Client.client.user.displayAvatarURL()
    };
    return embed;
  }
}

exports.commissions = {
  errors: {
    noTicketChannel: `**Invalid Channel** => You must be in a \`ticket\` channel to execute that command.`,
    noCommissionsChannel: `**Disabled Function** => To enable this feature, please create a \`commissions\` text channel.`,
    invalidPermissions: `**Invalid Permissions** => You must have the \`Arctic Representative\` role or Higher to execute that command.`,
    invalidRoleCheck: `**Invalid Role Provided** => A role that was entered does not exist. Role names are *cAsE sEnSeTiVe*. Exiting Commission Creation...`,
    noUserMentioned: `**Invalid Response** => You must mention a user to continue. Exiting Commission Creation...`,
    cancel: `**Exiting Commission System** => Canceled the Commission Process.`,
    invalidRoleClaim: `**Invalid Permissions** => You lack permissions to claim that commission.`,
    invalidArcticID: `**Invalid Syntax** => You must provide the ArcticID of a commission to execute that command.`,
    commissionNotFound: `**Invalid ID** => The ID you have supplied does not exist. Please double check the ID and try again.`,
    channelNotExists: `**Invalid Commission** => The commission you attempted to claim is no longer active.`,
    noInfoFound: `**Invalid Command** => We are unable to find any commissions in this channel.`,
    invalidUnclaim: `**Invalid Command** => This commission hasn't been claimed yet so can't be unclaimed.`,
    invalidUserUnclaim: `**Invalid Permissions** => You are not the most recent claimer of this commission.`,
    invalidEditType: `**Invalid Syntax** => The type that you entered is not valid. Exiting Edit Process...`
  },
  create: {
    welcome: `**Arctic Commissions System**\n \nWelcome to Arctic Commissions. In this dialogue we will ask questions about the commission you want to post.`
      + `\n \nWant to Quit? Just type \`cancel\` at any time or wait 100 seconds.`
      + `\n \nFirstly, please type the name of the role related to your commission. (E.G // Bot Development, Graphics, etc).`,
    budget: `**Arctic Commissions System**\n \nPlease enter the budget for this project. (E.G // $5, $500, Quote, etc).`,
    timeframe: `**Arctic Commissions System**\n \nPlease enter the timeframe for this proejct. (E.G // 1 week, 3 days, etc).`,
    description: `**Arctic Commissions System**\n \nPlease enter any deatails for this project. (E.G // A Logo with a flower, etc).`,
    client: `**Arctic Commissions System**\n \nFinally, please @mention the Client. (E.G // @Dectom#0001, @Arctic#0001, etc)`,
    complete: () => {
      let embed = {
        title: `**Commission Completed**`,
        description: `Thanks for using the commission system. We have posted this commission. Please wait patiently for a freelancer to claim it.`
      }
      return embed;
    },
    confirmation: (Client, commission) => {
      let embed = {
        title: `**New Commission for ${commission.client.username}**`,
        description: `Please check the following information then reply with \`confirm\` if everything is correct and the commission will be posted.`,
        fields: [
          { name: `Description`, value: `${commission.info.description}`, inline: true },
          { name: `Budget`, value: `${commission.info.budget}` },
          { name: `Timeframe`, value: `${commission.info.timeframe}` },
          { name: `Client`, value: `${commission.client.username}` }
        ]
      }
      return embed;
    },
    postCommission: (Client, msg, commission) => {
      let embed = {
        title: `New commission for ${commission.info.role}`,
        description: `**Description**:\n${commission.info.description}`,
        fields: [
          { name: `Arctic ID`, value: `\`${commission.arcticID}\`` },
          { name: `Timeframe`, value: commission.info.timeframe },
          { name: `Budget`, value: commission.info.budget },
          { name: `Client`, value: commission.client.username }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      }
      return embed;
    }
  },
  claim: {
    edit: (Client, commission, freelancer) => {
      let embed = {
        title: `Commission Claimed`,
        description: `This commission was claimed by ${freelancer.username}`,
        thumbnail: freelancer.displayAvatarURL()
      }
      return embed;
    },
    claimed: (Client, guild, commission, freelancer) => {
      let embed = {
        title: `**Commission Claimed** *(${commission.arcticID})*`,
        description: `Your commission has been claimed by ${freelancer.username}. Please provide them with any more information that you have so the freelancer can start working on your commission.`,
        thumbnail: freelancer.displayAvatarURL()
      }
      return embed;
    },
    notFound: (Client, id) => {
      let response = `**Commission Claimed** => That commission is already claimed.`;
      return response;
    }
  },
  unclaim: {
    confirmation: (Client, commission) => {
      let embed = {
        title: `Arctic Commissions System => ${commission.arcticID}`,
        description: `You are about to unclaim the following commission. Please check over the following information.\n \nIf you would like to proceed, please reply with \`confirm\` and the commission will be unclaimed and reposted.`,
        fields: [
          { name: `Description`, value: `${commission.commissionInfo.description}`, inline: true },
          { name: `Timeframe`, value: commission.commissionInfo.timeframe },
          { name: `Budget`, value: commission.commissionInfo.budget },
          { name: `Client`, value: commission.commissionClient.clientUserName }
        ]
      }
      return embed;
    },
    multiChoice: (Client, count) => {
      let embed = {
        title: `Arctic Commission Unclaiming`,
        description: `We found ${count} commissions in this channel. Please reply with the number of the commission you want within 10 seconds to make a choice.`,
        fields: []
      }
      return embed;
    },
    complete: (freelancer) => {
      let embed = {
        title: `Unclaiming Complete`,
        description: `The commission has been unclaimed and reposted. If the freelancer ${freelancer.username} is no longer required in this ticket, please execute \`-remove ${freelancer.username}\` to remove them from the ticket.`
      }
      return embed;
    }
  },
  repost: {
    confirmation: (Client, commission) => {
      let embed = {
        title: `Arctic Commissions System => ${commission.arcticID}`,
        description: `You are about to repost the following commission. Please check over the following information.\n \nIf you would like to proceed, please reply with \`confirm\` and the commission will be reposted.\n \nIf you would like to edit some information before reposting, please use the \`-commission edit\` command.`,
        fields: [
          { name: `Description`, value: `${commission.commissionInfo.description}`, inline: true },
          { name: `Timeframe`, value: commission.commissionInfo.timeframe },
          { name: `Budget`, value: commission.commissionInfo.budget },
          { name: `Client`, value: commission.commissionClient.clientUserName }
        ]
      }
      return embed;
    },
    multiple: (Client, count) => {
      let embed = {
        title: `Arctic Commission Reposting`,
        description: `We found ${count} commissions in this channel. Please reply with the number of the commission you want within 10 seconds to make a choice.`,
        fields: []
      }
      return embed;
    },
    complete: (freelancer) => {
      let embed = {
        title: `Reposting Complete`,
        description: `The commission has been reposted. Our freelancers have been notified and you should hear back soon.`
      }
      return embed;
    }
  },
  delete: {
    confirmation: (Client, commission) => {
      let embed = {
        title: `**Deleting Commission** => ${commission.arcticID}`,
        description: `You are about to delete commission ${commission.arcticID}. If you would wish to continue, please reply with \`confirm\` within 5 seconds to delete the commission.`,
        thumbnail: Client.client.user.displayAvatarURL(),
        fields: [
          { name: `Description`, value: commission.commissionInfo.description, inline: true },
          { name: `Roles`, value: commission.commissionInfo.role },
          { name: `Timeframe`, value: commission.commissionInfo.timeframe },
          { name: `Budget`, value: commission.commissionInfo.budget },
          { name: `Client`, value: commission.commissionClient.clientUserName }
        ]
      }
      return embed;
    },
    deleteUpdate: (Client, commission) => {
      let embed = {
        title: `Deleted Commission`,
        description: `This commission has been deleted.`,
        thumbnail: Client.client.user.displayAvatarURL()
      }
      return embed;
    },
    multi: (Client, count) => {
      let embed = {
        title: `Arctic Commissions List`,
        description: `We found ${count} commissions in this channel. Please reply with the number of the commission you want to delete within 10 seconds.`,
        fields: []
      }
      return embed;
    },
    deleted: (Client, commission) => {
      let embed = {
        title: `Deleted Arctic Commission`,
        description: `Successfully Deleted Arctic Commission ${commission.arcticID}`
      }
      return embed;
    }
  },
  info: {
    notFound: (Client, id) => {
      let response = `**Invalid ID** => We couldn't find a commission matching that ID in our database. Please try again.`;
      return response;
    },
    commissionInfo: (Client, commission) => {
      let embed = {
        title: `**Commission Info** => ${commission.arcticID}`,
        thumbnail: Client.client.user.displayAvatarURL(),
        fields: [
          { name: `Description`, value: commission.commissionInfo.description, inline: true },
          { name: `Roles`, value: commission.commissionInfo.role },
          { name: `Timeframe`, value: commission.commissionInfo.timeframe },
          { name: `Budget`, value: commission.commissionInfo.budget },
          { name: `Client`, value: commission.commissionClient.clientUserName }
        ]
      }
      return embed;
    },
    choice: (Client, count) => {
      let embed = {
        title: `Arctic Commission Info`,
        description: `We found ${count} commissions in this channel. Please reply with the number of the commission you want within 10 seconds to make a choice.`,
        fields: []
      }
      return embed;
    }
  },
  edit: {
    intro: (Client, commission) => {
      let embed = {
        title: `Arctic Commission Editing => ${commission.arcticID}`,
        description: `Welcome to Arctic Commission Editing. In this dialogue we will give you the ability to edit any feature of a currently posted commission, just follow the dialogue.` +
          `\n \nWant to Quit? Just type \`cancel\` at any time or wait 100 secconds.` +
          `\n \nPlease read the following commission info. If this is the correct commission reply with \`confirm\` to start editing.`,
        fields: [
          { name: `Description`, value: commission.commissionInfo.description, inline: true },
          { name: `Timeframe`, value: commission.commissionInfo.timeframe },
          { name: `Budget`, value: commission.commissionInfo.budget },
          { name: `Client`, value: commission.commissionClient.clientUserName },
          { name: `Roles`, value: commission.commissionInfo.role }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    },
    singleChoice: (Client, commission) => {
      let embed = {
        title: `Arctic Commission Editing => ${commission.arcticID}`,
        description: `Please choose an item from below by replying with the item title.`,
        fields: [
          { name: `Description`, value: commission.commissionInfo.description, inline: true },
          { name: `Timeframe`, value: commission.commissionInfo.timeframe },
          { name: `Budget`, value: commission.commissionInfo.budget },
          { name: `Roles`, value: commission.commissionInfo.role }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    },
    multiChoice: (Client, count) => {
      let embed = {
        title: `Arctic Commission Editing`,
        description: `We found ${count} commissions in this channel. Please reply with the number of the commission you want within 10 seconds to make a choice.`,
        fields: []
      }
      return embed;
    },
    editType: (Client, commission, type) => {
      let message = type.split('');
      let first = message[0].toUpperCase();
      message.shift();
      let rest = message.join("");
      message = first + rest;
      let embed = {
        title: `Arctic Commission Editing => ${commission.arcticID}`,
        description: `You have chosen to edit the \`${type}\`. Below is the current value of the \`${type}\`\n \nPlease reply with what you want the new value to be.`,
        fields: [
          { name: `${message}`, value: commission.commissionInfo[type] }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      }
      return embed;
    },
    confirmation: (Client, commission, type, content) => {
      let embed = {
        title: `Arctic Commission Editing => ${commission.arcticID}`,
        description: `You have edited the \`${type}\` value.\n \nPlease check over the commission information and reply with \`confirm\` to complete the edit stage.`,
        fields: [
          { name: `Description`, value: content, inline: true },
          { name: `Timeframe`, value: commission.commissionInfo.timeframe },
          { name: `Budget`, value: commission.commissionInfo.budget },
          { name: `Roles`, value: commission.commissionInfo.role }
        ]
      };
      return embed;
    },
    complete: (Client, commission, type) => {
      let embed = {
        title: `Arctic Commission Editing => ${commission.arcticID}`,
        description: `You have successfully edited the \`${type}\` of  Commission ${commission.arcticID}.\n \nTo update the commission to freelancers please run \`-commission repost\`.`
      }
      return embed;
    }
  }
}

exports.quotes = {
  errors: {
    noQuotesChannel: `**Disabled Command** => This command is disabled, please create the \`quotes\` channel to enable it.`,
    notTicketChannel: `**Incorrect Channel** => This command must be used in a ticket channel.`,
    cancel: `**Exiting Quotes System** => Cancelling the Quotes system...`
  },
  create: {
    welcome: `**Arctic Studios Quotes**\n \nWelcome to Arctic Studios. This dialogue will take you through the stages of creating and posting a quote\n \nWant to Quit? Type \`cancel\` at any time or wait 100 secconds.\n \nPlease enter the role related to your project. (E.G // Bot Development, Graphics, etc)`,
    description: `**Arctic Studios Quotes**\n \nPlease enter a brief description of the product you want a quote for. (E.G // A Discord bot that creates quotes, etc)`,
    timeframe: `**Arctic Studios Quotes**\n \nPlease enter the timeframe for this project. (E.G // 2 days, 1 month, etc)`,
    confirmation: (Client, quote) => {
      let embed = {
        title: `Arctic Studios Quotes`,
        description: `Please read over the following information then reply with \`confirm\` if everything is okay.`,
        fields: [
          { name: `Roles`, value: quote.info.roles },
          { name: `Description`, value: quote.info.description },
          { name: `Timeframe`, value: quote.info.timeframe }
        ]
      };
      return embed;
    },
    complete: `**Arctic Studios Quote**\n \nYour quote has been posted. Please wait patiently whilst our team calculates your quote.`
  }
}

exports.staff = {
  errors: {
    noHRTicket: `**Invalid Channel** => That command can only be executed in a \`hr-ticket\` channel.`,
    noTicket: `**Invalid Channel** => That command can only be executed in a \`ticket\` channel.`,
    missingStaffLog: `**Disabled Command** => That command requires the \`#staff-watch\` channel to be executed.`,
    invalidPermissions: `**Invalid Permissions** => You must have the \`Human Resources\` role or Higher to execute that command.`,
    invalidClientPermissions: `**Invalid Permissions** => You must have the \`Clients\` role to execute that command.`,
    missingStaffTag: `**Invalid Syntax** => You must @mention a staff member to manage them.`,
    cancel: `**Exiting Staff Process** => Canceled the Staff Process...`,
    cancelRemove: `**Exiting Staff Removal Process** => Canceled the Staff Removal Process...`,
    invalidRoleCheck: `**Invalid Role Provided** => A role that was entered does not exist. Role names are *cAsE sEnSeTiVe*. Exiting Staff Creation Process...`,
    missingStaffWatch: `**Command Error** => This command requires the \`staff-watch\` channel.`,
    invalidStaffMember: `**Invalid Syntax** => The user mentioned isn't in the Arctic Team.`,
    invalidArcticID: `**Invalid Syntax** => The ArcticID provided was not found in our Database. Exiting Reviews System...`,
    tooHighRating: `**Invalid Syntax** => You entered a rating above 10. Exiting reviews process...`,
    userAlreadyStaff: (Client, staff) => {
      let response = `**Invalid Syntax** => The user \`${staff.tag}\` is already a staff member.`;
      return response;
    },
    noPortfolioType: `**Invalid Syntax** => That command requires a type. (E.G // web, Dectom, development, graphics, etc)`,
    invalidPortfolioType: `**Invalid Syntax** => That portfolio type/user was not found.`,
    notStaffMember: `**Invalid Permissions** => You must be an Arctic Studios Staff Member to execute that command.`,
    invalidEditOption: `**Invalid Syntax** => The type you entered is not valid. Exiting edit process...`,
    invalidPermissionsEditOption: `**Invalid Syntax** => You must have the \`Human Resources\` role or Higher to edit that paramater. Exiting edit process...`,
    invalidProfile: `**Invalid Profile** => Please enter the name of an Arctic Studios Freelancer.`,
    roleTooLow: `**Invalid Permissions** => You require the \`Arctic Representative\` role or Higher to execute that command.`,
    noArcticStaff: `**Invalid Command** => There are currently no Arctic Studios staff members to be found.`
  },
  create: {
    welcome: `**Arctic Staff Creation**\n \nWelcome to the Arctic Staff Creation Process. In this dialogue we will ask questions about the Staff Member that you want to add to Arctic.`
      + `\n \nWant to Quit? Just type \`cancel\` at any time or wait 100 secconds.`
      + `\n \nFirstly, Please type the name of the roles that the freelancer is applying for. If it's multiple roles, seperate them by a comma. *cAsE sEnSeTiVe* (E.G // Bot Development, Graphics, Web Development, etc)`,
    paypal: `**Arctic Staff Creation**\n \nPlease enter the Freelancers current Paypal E-Mail Address. (E.G // freelancer@arcticstudios.com, etc)`,
    portfolio: `**Arctic Staff Creation**\n \nPlease enter the Freelancers current Portfolio Site Address. (E.G // https://arcticstudios.org, etc)`,
    confirm: (Client, msg, params, args) => {
      let embed = {
        title: `${args.potentialStaff.username}'s Staff Creation`,
        description: `Please check the following information then reply with \`confirm\` if everything is correct and the new staff member will be assigned there roles and entered into the database.`,
        fields: [
          { name: `Roles`, value: `\`${args.roleArray.join(", ")}\``, inline: true },
          { name: `Paypal`, value: `${args.paypal}` },
          { name: `Portfolio`, value: `${args.portfolio}` }
        ]
      }
      return embed;
    },
    staffWatch: (Client, staff) => {
      let embed = {
        title: `[${staff.type === "success" ? "+" : "-"}] ${staff.user.tag}`,
        thumbnail: staff.user.displayAvatarURL(),
        description: staff.roles.join(", ")
      }
      return embed;
    },
    complete: (Client, staff) => {
      let embed = {
        title: `Welcome to Arctic`,
        description: `Thanks for completing the Arctic Staff Creation process and a welcome to ${staff.user.tag} our newest ${staff.roles.join(", ")}`,
        thumbnail: staff.user.displayAvatarURL()
      }
      return embed;
    },
    rehire: (Client, msg, params, args) => {
      let embed = {
        title: `Rehiring ${args.potentialStaff.tag}`,
        description: `The user that you have mentioned has been staff on Arctic Studios before. They were removed at \`${args.staffMember.staffExtra.staffFireAt}\`. If you would like to continue in hiring this staff member.\n \nPlease reply with \`confirm\` and there profile will be re-instated.`,
        fields: [
          { name: `Roles`, value: args.staffMember.staffPersonals.staffRoles },
          { name: `Portfolio`, value: args.staffMember.staffPersonals.staffPortfolio },
          { name: `Paypal`, value: args.staffMember.staffPersonals.staffPaypal },
          { name: `Commissions Claimed`, value: !args.staffMember.staffCommissions ? '0' : args.staffMember.staffCommissions.length }
        ]
      }
      return embed;
    }
  },
  remove: {
    confirmation: (Client, staff) => {
      let embed = {
        title: `Removal of ${staff.user.username}`,
        description: `You are about to remove \`${staff.user.tag}\` from Arctic Studios. Please read over there details then reply with \`confirm\` if you wish to delete the staff member.`,
        fields: [
          { name: `Roles`, value: staff.roles },
          { name: `Portfolio`, value: staff.profile.staffPersonals.staffPortfolio },
          { name: `Paypal`, value: staff.profile.staffPersonals.staffPaypal },
          { name: `Claimed Commissions`, value: !staff.profile.staffCommissions ? '0' : staff.profile.staffCommissions.length }
        ]
      }
      return embed;
    },
    complete: (Client, staff) => {
      let embed = {
        title: `Removed From Arctic`,
        description: `Successfully Removed \`${staff.user.tag}\` from Arctic Studios.`,
        thumbnail: staff.user.displayAvatarURL()
      }
      return embed;
    }
  },
  review: {
    notClaimed: `**Arctic Reviews System** => This commission has not been claimed by a freelancer, therefore it cannot be reviewed.`,
    welcome: `**Arctic Reviews System**\n \nWelcome to Arctic Reviews. In this dialogue we will ask questions about your experience with Arctic Studios so you can leave feedback and a review on us and the freelancer.`
      + `\n \nWant to Quit? Just type \`cancel\` at any time or wait 100 secconds.`
      + `\n \nTo continue, please say \`continue\``,
    freelancer: (Client, commission, value) => {
      let embed = {
        description: `**Arctic Reviews System**\n \nThanks, below you can see the commission that we have found. We linked the following freelancer with the commission.\n \nIf this is correct, please reply with \`confirm\` or the username of the freelancer that completed your commission. *cAsE sEnSeTiVe* (E.G // Dectom, Jordan, etc)`,
        fields: [
          { name: `Freelancer`, value: `${commission.commissionClaimed[value].staffName} (${commission.commissionClaimed[value].staffID})` }
        ]
      }
      return embed;
    },
    multiChoice: (Client, count) => {
      let embed = {
        title: `Arctic Commission Review`,
        description: `We found ${count} commissions in this channel. Please reply with the number of the commission you want within 10 seconds to make a choice.`,
        fields: []
      }
      return embed;
    },
    rating: `**Arctic Reviews System**\n \nPlease give your freelancer a rating out of 10. *Whole numbers only* (E.G // 9, 10, 6, etc)`,
    description: `**Arctic Reviews System**\n \nPlease give a detailed review of your experience. **Will be posted publicly** (E.G // Worked fast and got what I wanted, etc)`,
    confirm: (Client, commission, args) => {
      let embed = {
        title: `New Review from ${args.reviews.author.tag}`,
        description: `Please check the following information then reply with \`confirm\` if everything is correct and the review will be posted.`,
        fields: [
          { name: `Description`, value: args.reviews.description, inline: true },
          { name: `Commission ID`, value: args.reviews.arcticID },
          { name: `Freelancer`, value: args.reviews.freelancer.staffName },
          { name: `Rating`, value: args.reviews.rating }
        ]
      }
      return embed;
    },
    posted: (Client, msg, args) => {
      let embed = {
        title: `Review by ${args.reviews.author.tag}`,
        fields: [
          { name: `Description`, value: args.reviews.description, inline: true },
          { name: `Commission ID`, value: args.reviews.arcticID },
          { name: `Freelancer`, value: args.reviews.freelancer.staffName },
          { name: `Rating`, value: args.reviews.rating }
        ],
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      }
      return embed;
    },
    complete: `**Arctic Reviews System**\n \nThanks for posting your review and sharing your experience.`
  },
  portfolio: {
    staffMember: (Client, staff) => {
      let embed = {
        title: `${staff.staffName}'s Portfolio`,
        description: `Below is the portfolio for \`${staff.staffName}#${staff.staffDiscrim}\`.`,
        fields: [
          { name: `Roles`, value: `${staff.staffPersonals.staffRoles}` },
          { name: `Portfolio`, value: `${staff.staffPersonals.staffPortfolio}` },
          { name: `Last Active`, value: `${Client.utils.timeAgo(staff.staffExtra.staffLastActive)}` }
        ],
        thumbnail: staff.staffProfilePicture
      }
      return embed;
    },
    type: (Client, type, profiles) => {
      let embed = {
        title: `Arctic Portfolios => \`${type}\``,
        description: `We found ${profiles.length} profiles that matched your portfolio search.`,
        fields: []
      };
      return embed;
    }
  },
  edit: {
    options: (Client, staff) => {
      let embed = {
        title: `Arctic Staff Edits => ${staff.arcticID}`,
        description: `Welcome to the Staff Editing Process. In this dialogue you will go about editing \`${staff.staffName}\`'s Arctic Studios staff Profile.\n \nWant to Quit? Just type \`cancel\` at any time or wait 100 secconds. \n \nPlease choose an item from below by replying with the item title.`,
        fields: [
          { name: `Paypal`, value: staff.staffPersonals.staffPaypal },
          { name: `Roles`, value: staff.staffPersonals.staffRoles },
          { name: `Portfolio`, value: staff.staffPersonals.staffPortfolio }
        ],
        thumbnail: staff.staffProfilePicture
      };
      return embed;
    },
    editValue: (Client, staff, type, value) => {
      let embed = {
        title: `Arctic Staff Edits => ${staff.arcticID}`,
        description: `You have chosen to edit \`${type}\` Please reply with the new value.`,
        fields: [
          { name: type, value: staff.staffPersonals[value] }
        ]
      }
      return embed;
    },
    confirmation: (Client, staff, type, value, newValue) => {
      let embed = {
        title: `Arctic Staff Edits => ${staff.arcticID}`,
        description: `Are you sure you want to update \`${type}\`? Reply with \`confirm\` to process this change.`,
        fields: [
          { name: `Old Value`, value: value },
          { name: `New Value`, value: newValue }
        ]
      }
      return embed;
    },
    success: `**Arctic Staff Edits**\n \nSuccessfully edited a Arctic Staff profile.`
  },
  profile: {
    profile: (Client, staff) => {
      let embed = {
        title: `${staff.staffName}'s Arctic Profile`,
        description: `${staff.staffPersonals.staffRoles}`,
        fields: [
          { name: `Portfolio`, value: staff.staffPersonals.staffPortfolio },
          { name: `Commissions Claimed`, value: staff.staffCommissions.length },
          { name: `Hired at`, value: staff.staffExtra.staffHireAt }
        ],
        thumbnail: staff.staffProfilePicture,
        footer: {
          text: staff.staffExtra.staffLastUpdated
        }
      };
      return embed;
    }
  },
  list: {
    list: (Client, list) => {
      let embed = {
        title: `Arctic Studios Freelancer List`,
        description: `A list of all our ${list.length} freelancers and there portfolios. To get a more specific range of freelancers please use our \`-staff portfolio\` command.`,
        fields: [],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    }
  }
}

exports.partners = {
  errors: {
    invalidPermissions: `**Invalid Permissions** => You must have the \`Board of Directors\` role to execute that command.`,
    userNotPartner: `**Invalid Permissions** => You must have the \`Partners\` role to execute that command.`,
    alreadyPartner: `**Something here** => The mentioned user is already a partner.`,
    notAPartner: `**Something here** => The mentioned user is not a partner.`,
    cancel: `**Exiting Parnters System** => Cancelled the Partners Process.`,
    invalidPartnerName: `**Invalid Syntax** => Please enter the company name of the partnership you wish to delete.`,
    mentionAPartner: `**Invalid Syntax** => Please type the name of a partnered company to get its info.`,
    noPartnersFound: `**System Error** => We were unable to find any Arctic Studios partnerships at this time.`
  },
  create: {
    enterUsername: `**Arctic Studios Partnership Creation**\n \nWelcome to the Arctic Partners Programme. In this dialogue we will go through the stages of initializing a partnership by collecting information on the proposed partnership.` +
      `\n \nWant to Quit? Just type \`cancel\` at any time or wait 100 secconds.\n \nPlease begin by typing the username of the member we are partnering with. (E.G // Dectom, Jordan, etc)`,
    companyName: `**Arctic Studios Partnership Creation**\n \nPlease enter the name of the new partners company/business. (E.G // Arctic Studios, Dectom.xyz, etc)`,
    website: `**Arctic Studios Partnership Creation**\n \nPlease enter the URL to the new Partners Website. (E.G // https://arcticstudios.org, etc)`,
    discord: `**Arctic Stuidos Partnership Creation**\n \nPlease enter the new Partners Discord Invite URL.`,
    description: `**Arctic Studios Partnership Creation**\n \nPlease enter a brief description about the new Partner.`,
    advert: `**Arctic Studios Partnership Creation**\n \nPlease enter the set advertisement for the partnership. (Can be edited at a later date. Don't include the Discord URL)`,
    confirmation: (Client, args) => {
      let embed = {
        title: 'Arctic Studios Partnership Creation',
        description: `Please look over the following information and reply with \`confirm\` if it is correct.`,
        fields: [
          { name: `Name`, value: args.partner.name },
          { name: `Website`, value: args.partner.website },
          { name: `Discord`, value: args.partner.discord },
          { name: `Description`, value: args.partner.description }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    },
    complete: `**Arctic Studios Partnership**\n \nThanks for Joining Arctic Studios Partner Programme.`
  },
  edit: {
    confirmation: (Client, partner) => {
      let embed = {
        title: `Arctic Partnership Editing => ${partner.partnerInfo.name}`,
        description: `Please reply with the title of a section to start editing it.`,
        fields: [
          { name: `Description`, value: partner.partnerInfo.description, inline: true },
          { name: `Name`, value: partner.partnerInfo.name },
          { name: `Website`, value: partner.partnerInfo.website },
          { name: `Discord`, value: partner.partnerInfo.discord },
          { name: `Advert`, value: partner.partnerInfo.advert }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    },
    editType: (Client, partner, type) => {
      let message = type.split('');
      let first = message[0].toUpperCase();
      message.shift();
      let rest = message.join("");
      message = first + rest;
      let embed = {
        title: `Arctic Partnership Editing => ${partner.partnerInfo.name}`,
        description: `You have chosen to edit the \`${type}\`. Below is the current value of the \`${type}\`\n \nPlease reply with what you want the new value to be.`,
        fields: [
          { name: `${message}`, value: partner.partnerInfo[type] }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      }
      return embed;
    },
  },
  delete: {
    confirmation: (Client, partner) => {
      let embed = {
        title: `Arctic Partnership Deletion => ${partner.partnerID}`,
        description: `Please read over the following information then reply with \`confirm\` to continue the process.`,
        fields: [
          { name: `Description`, value: partner.partnerInfo.description, inline: true },
          { name: `Name`, value: partner.partnerInfo.name },
          { name: `Website`, value: partner.partnerInfo.website },
          { name: `Discord`, value: partner.partnerInfo.discord },
          { name: `Advert`, value: partner.partnerInfo.advert }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    },
    complete: `**Arctic Partnerhship Deletion**\n \nSuccessfully Deleted the partnership.`
  },
  info: {
    message: (Client, partner) => {
      let embed = {
        title: `Arctic Partnership => ${partner.partnerID}`,
        description: `The Following is our information on the partner ${partner.partnerInfo.name}`,
        fields: [
          { name: `Description`, value: partner.partnerInfo.description, inline: true },
          { name: `Website`, value: partner.partnerInfo.website },
          { name: `Discord`, value: partner.partnerInfo.discord },
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    }
  },
  list: {
    message: (Client, length) => {
      let embed = {
        title: `Arctic Studios Partnerships`,
        description: `Below is a list of Arctic partners. We have a total of ${length} dedicated partners that we work closely with.`,
        fields: [],
        thumbnail: Client.client.user.displayAvatarURL()
      }; 
      return embed;
    }
  }
}

exports.games = {
  errors: {
    tooLowStake: `**Invalid Syntax** => You entered a stake too low. Minimum stake is \`25\` coins.`,
    invalidStake: `**Invalid Syntax** => You staked an amount you don't have.`,
    gameInProgress: `**Invalid Permissions** => You already have a game running. You may only play one game at a time.`,
    noGameInProgress: `**Invalid Usage** => You don't have any active games to clear.`
  },
  blackjack: {
    game: (Client, bj) => {
      let embed = {
        title: `Blackjack Game => ${bj.user.username}`,
        description: bj.description,
        fields: [
          { name: `${bj.user.username}'s Hand`, value: `${bj.hands.playerHand.message.join(" - ")}\nValue: ${bj.hands.playerHand.value}` },
          { name: `Dealers Hand`, value: `${bj.hands.dealerHand.message.join(" - ")}\nValue: ${bj.hands.dealerHand.value}` }
        ],
        footer: {
          text: `Stake: ${bj.stake} | Winnings ${bj.winnings} | Cards Left ${bj.decks.activeDeck.length}`
        }
      };
      return embed;
    },
    info: `**Arctic Studios Blackjack**\n \nWelcome to Blackjack. You can reply with \`hit\` to draw another card or \`stand\` to pass your turn. The split is in development as they don't play that where I come from.`,
    goodMessages: [
      `Aim for the stars my dude.`,
      `Winner winner chicken dinner`,
      `You win`,
      `I guess cheaters do win`,
      `Yeeeeeeeeeeeeeeeeeeeeet`,
      `Awesome Possum`,
      `Jesus Christ Superstar!`,
      `Victory Royale`,
      `What do you want a medal?`,
      `Congrats, You can count to 21`,
      `Did you even try?`
    ],
    badMessages: [
      `Unlucky`,
      `The dealer dealed a loss`,
      `Looks like the dealer has done it again.`,
      `Its about skill, luck, commitment and cards up the sleeve`,
      `I'm just too good`,
      `Call me, beep me, if you wonna beat me`,
      `Improvise. Adapt. Overcome.`,
      `I'm lovin it.`,
      `Finger Lickin Good`
    ]
  },
  slots: {
    info: `**Arctic Studios Slots**\n \nWelcome to Slots, to play use the command \`-slots <stake>\` to stake an amount from your balance. If you match at least two on the same row, you win 3/4 of your stake back to try again. To win, match 3 of the same type in a row.\n ` +
      `\n🍋 Lemons, 2x Multiplier` +
      `\n🍇 Grapes, 2x Multiplier` +
      `\n🍓 Strawberries, 2x Multiplier` +
      `\n🍉 Watermellon, 2x Multiplier` +
      `\n🍒 Cherries, 3x Multiplier` +
      `\n💰 Money, 4x Multiplier` +
      `\n🔷 Diamonds, 5x Multiplier`,
    game: (Client, game, user) => {
      let embed = {
        title: `${user.username}'s Arctic Slots`,
        description: `${game.winAmount > 0 ? `Congrats you won ${game.winAmount} coins.` : 'Better luck Next time'}\n \n` +
          `${game.activeMachine[0][0].symbol} | ${game.activeMachine[0][1].symbol} | ${game.activeMachine[0][2].symbol}\n` +
          `${game.activeMachine[1][0].symbol} | ${game.activeMachine[1][1].symbol} | ${game.activeMachine[1][2].symbol}\n` +
          `${game.activeMachine[2][0].symbol} | ${game.activeMachine[2][1].symbol} | ${game.activeMachine[2][2].symbol}`
      };
      return embed;
    }
  },
  cockFight: {
    errors: {
      invalidChannel: `**Invalid Channel** => That command is locked to a bot-commands channel`,
      invalidStake: `**Invalid Stake** => You staked an amount you don't have.`,
      minimumStake: `**Invalid Stake** => The minimum stake is 25`
    },
    welcome: (Client, msg, args, params, currentProfile) => {
      let embed = {
        title: `__**Arctic CockFights**__`,
        description: `Welcome to CockFight. Where your cock battles against the AI Cock to come out victorious in a grousome battle to the death between innocent chickens named Albert.\n`
          + `\n**Playing the Game**\n`
          + `\`${Client.tokens.prefix}cockfight <stake>\` | \`${Client.tokens.prefix}cf <stake>\`\n`
          + `\n**How to Play**\n`
          + `- If your cock wins your chance goes down by 5%.\n`
          + `- Starting at 50% going down to 25%\n`
          + `- Each win increases your multiplier by 0.50x.\n`
          + `- Starting at 2x to a maximum of 4.5x\n`
          + `\n**Your Cock**\n`
          + `Name: ${currentProfile.cockInfo.name} | Total Wins: ${currentProfile.cockInfo.totalWins === undefined ? 0 : currentProfile.cockInfo.totalWins}\n`
          + `Chance: ${currentProfile.cockInfo.chance}% | Multiplier: ${currentProfile.cockInfo.multiplier}x`
      }
      return embed;
    },
    game: (Client, msg, args, params, cockFightProfile) => {
      let embed = {
        title: `**${msg.author.username}'s CockFight**`,
        description: `${cockFightProfile.msg}`,
        thumbnail: Client.client.user.displayAvatarURL(),
        fields: [
          { name: `Cock Name`, value: cockFightProfile.cockInfo.name },
          { name: `Total Wins`, value: cockFightProfile.cockInfo.totalWins },
          { name: `Chance`, value: `${cockFightProfile.cockInfo.chance}%` },
          { name: `Multiplier`, value: `${cockFightProfile.cockInfo.multiplier}x` }
        ],
        footer: {
          text: `Stake: ${args.stake} | Winnings: ${cockFightProfile.winnings}`
        }
      }
      return embed;
    }
  },
  flip: {
    info: `**Arctic Coinflip**\n \nChoose either \`heads\` or \`tails\` and win double your money if you guess correct.`,
    game: (Client, game) => {
      let embed = {
        title: `${game.user.username}'s Coinflip Game`,
        description: game.result,
        footer: {
          text: `Stake: ${game.stake} | Winnings: ${game.winnings}`
        }
      };
      return embed;
    }
  },
  trivia: {
    info: `**Arctic Studios Trivia**\n \nWelcome to Arctic Studios Trivia. Test your knowlege. To start a game execute the command with an amount to stake. \`-trivia <25>\`.\n \nThe Trivia Game will randomly choose a category for you and then present you with 5 questions. These questions can either be multiple choice, or true or false questions. Read the question carefully then reply with the number for the operation you would like to choose.\n \nYou have a maximum of 15 secconds per question. If you time elapses you will get a notification and the next question will appear.\n \n**Multipliers**\n1.25x => 1 Question Correct\n1.50x => 2 Questions Correct\n1.75x => 3 Questions Correct\n2.00x => 4 Questions Correct\n3.00x => 5 Questions Correct`,
    confirmation: (Client, trivia) => {
      let embed = {
        title: `${trivia.user.username}'s Trivia Game`,
        description: `Your category has been chosen and your game is ready. Reply with \`confirm\` to start your trivia game.`,
        footer: {
          text: `Stake: ${trivia.stake} => Category: ${trivia.category.name}`
        }
      };
      return embed;
    }
  },
  chatGames: {
    
  },
  scratchCard: {
    invalidStake: `**Invalid Stake** => You need a minimum of $25,000 to purchase a scratchcard.`,
    confirmation: (Client, args) => {
      let embed = {
        title: `${args.user.username}'s ScratchCard`,
        description: `Welcome to Arctic's Scratchcard.\n \nScratchcards cost $25,000 to play but you can win rewards up to $10 Million.\n \nTo confirm that you want to use a scratchcard, please react with :white_check_mark:`,
        image: "https://i.dectom.xyz/081d8448.png"
      };
      return embed;
    }
  }
}

exports.economy = {
  errors: {
    noProfile: `**Invalid Permissions** => The user suplied doesn't have a profile in the Database. Please get them to send a message then try again.`,
    invalidUser: `**Invalid Syntax** => The user provided couldn't be found on Arctic Studios Discord.`,
    invalidStake: `**Invalid Syntax** => You attempted to send an amount you don't have.`,
    invalidUser: `**Invalid Syntax** => You must enter the name of the user you want to send the coins to.`,
    tooLowPay: `**Invalid Syntax** => You entered an amount that is too low. Minimum amount is \`25\` coins`,
    notEnoughHistory: `**Invalid Permissions** => You need to make atleast 15 transactions to view your history.`
  },
  profile: {
    fetchingProfile: `**Arctic Profiles** => Fetching Arctic profile...`
  },
  profileCard: (Client, profile) => {
    let embed = {
      author: {
        name: `${profile.userName}'s Arctic Profile | ${profile.arcticID}`,
        icon: profile.userProfileLink
      },
      fields: [
        { name: `User Level`, value: profile.userLevel },
        { name: `Level Progress`, value: `${profile.userLevelProgress} / ${profile.userLevel * 50}` },
        { name: `Coins`, value: profile.userBalance }
      ]
    }
    return embed;
  },
  leaderboard: {
    base: (Client) => {
      let embed = {
        title: `Arctic Studios Leaderboard`,
        description: `The top 10 members of Arctic that are dedicated and active. Here you can find who has the highest level and XP.`,
        fields: [],
        thumbnail: Client.client.user.displayAvatarURL()
      }
      return embed;
    },
    baseBal: (Client) => {
      let embed = {
        title: `Arctic Studios Leaderboard`,
        description: `The top 10 members of Arctic that are great with there coins. They are skilled users that can play the odds and have the best luck on our games.`,
        fields: [],
        thumbnail: Client.client.user.displayAvatarURL()
      }
      return embed;
    }
  },
  pay: {
    intro: `**Arctic Studios Economy**\n \nTo send money to another user please use the following format. \`-pay Dectom 25\` `,
    success: (Client, args, user) => {
      let embed = {
        title: `Arctic Studios Economy`,
        description: `Successfully Completed Transaction`,
        fields: [
          { name: `From`, value: `\`${args.author.tag}\`` },
          { name: `To`, value: `\`${user.user.tag}\`` },
          { name: `Amount`, value: `\`${args.stake}\`` }
        ],
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      };
      return embed;
    }
  },
  history: {
    eco: (Client, user) => {
      let embed = {
        title: `${user}'s Economy History`
      }; 
      return embed;
    }
  }
}

exports.giveaways = {
  errors: {
    invalidPerms: `**Invalid Permissions** => You lack the \`Board of Directors\` role reqiured to execute that command.`,
    noID: `**Invalid Syntax** => You must include the ArcticID for the giveaway.`,
    invalidChannel: `**Invalid Response** => You didn't mention a channel in your message. Exiting Giveaway Creation Process.`,
    invalidConfirm: `**Invalid Response** => You didn't reply with \`confirm\`. Exiting the Giveaway Creation Process.`
  },
  types: (Client) => {
    let response = `**Arctic Giveaway Types**\n \n` +
      `${Client.tokens.prefix}giveaway create => Create a giveaway\n` +
      `${Client.tokens.prefix}giveaway info <arcticID> => Info on a Giveaway\n` +
      `${Client.tokens.prefix}giveaway delete <arcticID> => Delete a Giveaway\n` +
      `${Client.tokens.prefix}giveaway end <arcticID> => End a Giveaway\n` +
      `${Client.tokens.prefix}giveaway redraw <arcticID> => Re-draw a Giveaway`
    return response;
  },
  create: {
    channel: `**Arctic Giveaway Creation**\n \nWelcome to the Arctic Giveaway creation. Please mention the channel you want the giveaway to run in. (E.G // #giveaways, etc)`,
    title: `**Arctic Giveaway Creation**\n \nThanks, Please enter the title for the giveaway. (E.G // $100 Giveaway, Free Discord Bot, etc)`,
    description: `**Arctic Giveaway Creation**\n \nGreat, Please now give a detailed description about the giveaway. (E.G // A Discord bot with music commands and moderation, etc)`,
    endDate: `**Arctic Giveaway Creation**\n \nAmazing, Please enter the time in minuetes that you want the giveaway to last. (E.G // 60, 1000, etc)`,
    tag: `**Arctic Giveaway Creation**\n \nFinally, Do you want the giveaway to tag everyone? (E.G // \`yes\`,\`no\`)`,
    confirmation: (Client, msg, params, args) => {
      let embed = {
        title: `**Arctic Giveaway** | ${args.giveawayID}`,
        fields: [
          { name: `Arctic ID`, value: `${args.giveawayID}` },
          { name: `End Date`, value: `${args.giveawayInfo.endDate} mins` },
          { name: `Channel`, value: `${args.giveawayInfo.channel}` },
          { name: `Title`, value: `${args.giveawayInfo.title}` },
          { name: `Description`, value: `${args.giveawayInfo.description}` }
        ]
      }
      return embed;
    },
    post: (Client, msg, params, args) => {
      let embed = {
        title: `**${args.giveawayInfo.title}**`,
        description: `${args.giveawayInfo.description}`,
        footer: {
          text: `ID: ${args.giveawayID} | End date: ${args.giveawayInfo.endDate} mins`
        }
      }
      return embed;
    }
  }
}

exports.referrals = {
  errors: {
    noUserTagged: `**Invalid Syntax** => You must tag a user to reffer them.`,
    alreadyReferred: `**Invalid Permissions** => You have already referred a user.`,
    noReferrals: `**No Referrals Found** => You have reffered no users.`
  },
  baseRef: (Client) => {
    let embed = {
      title: `Arctic Studios Referrals`,
      description: `A list of the top 10 Referrers in Arctic Studios.`,
      thumbnail: Client.client.user.displayAvatarURL(),
      fields: []
    }
    return embed;
  },
  list: (Client, user, refs) => {
    let embed = {
      title: `Arctic Studios Referrals => ${user.tag}`,
      description: `You have **${Math.floor(refs.totalReferrals.length / 2)}** referrals.`,
      thumbnail: user.displayAvatarURL()
    };
    return embed;
  }
}

exports.store = {
  errors: {
    invalidRole: `**Invalid Role** => This is not a valid role in Arctic Studios or the role is not whitelisted.`,
    invalidPermissions: `**Invalid Permissions** => You must have the \`Board of Directors\` role or higher to execute that command.`,
    invalidStoreItem: `**Invalid Syntax** => The ID for the item that you entered does not exist.`,
    failedDelete: `**Failed Deletion** => This item could not be deleted at this time.`,
    invalidItem: `**Invalid Item** => This is not a valid item in our store, please ensure you are using the correct \`itemID\``,
    invalidBuyID: `**Invalid Syntax** => To purchase an Item from the store please execute \`-buy <itemID>\`. If you are unsure of the items please execute \`-store\` for a full list of what we offer.`,
    notEnoughCoins: `**Invalid Syntax** => You lack the amount of coins needed to purchase that item.`,
    soldOutItem: `**Sold Out Item** => This item is no longer in stock.`
  },
  create: {
    addAction: `**Arctic Store Creation**\n\nWould you like to bind an action to this item? If not, please say \`no\`.\n**Available Actions:**\n\`addRole:role name here\`\n(ex: say \`addRole:VIP\`)`,
    cancel: `**Arctic Store Creation**\n\nSuccessfully cancelled item creation.`,
    welcome: `**Arctic Store Creation**\n \nWelcome to the Arctic Studios store item creation dialogue. In this process we will go through the stages necessary to create a new item to submit to the Arctic Studios Store.\n \nWant to Quit? Just type \`cancel\` at any time or wait 100 secconds.\n \nPlease enter the name of the item you wish to enter. \n(ex: 1 advertisement, VIP role, etc)`,
    quantity: `**Arctic Store Creation**\n \nPlease enter the quantity at which these are sold at.\n(ex: 1, 2, 3)`,
    description: `**Arctic Store Creation**\n \nPlease enter a brief description for the store item.\n(ex: allows access to post one advertisement, etc)`,
    price: `**Arctic Store Creation**\n \nPlease enter the price for the store item.\n(ex: 100, 4000, 50000)`,
    confirmation: (Client, args) => {
      let embed = {
        title: `Arctic Store Creation`,
        description: `Please read over the following information for your new store item. If everything is okay please reply with \`confirm\` to create the new store item.`,
        fields: [
          { name: `Description`, value: args.item.description, inline: true },
          { name: `Name`, value: args.item.name },
          { name: `Quantity`, value: parseInt(args.item.quantity).toLocaleString() },
          { name: `Price`, value: `$${args.item.price.toLocaleString()}` }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      if (args.item.action) embed.fields.push({ name: 'Action', value: `Binds \`${args.item.action.type}\` to \`${args.item.action.any}\`` });
      return embed;
    },
    complete: `**Arctic Studios Store Creation**\n \nThe item has been inserted to the database and ready to purchase.`
  },
  purchase: {
    confirmation: (Client, args) => {
      let embed = {
        title: `Item Purchase`,
        description: `Are you sure you'd like to purchase this item? If so, please say \`confirm\`.\nSay \`cancel\` if you do not wish to purchase this item.`,
        fields: [
          { name: `Name`, value: args.item.name, inline: true },
          { name: `Description`, value: args.item.description },
          { name: `Price`, value: parseInt(args.item.price).toLocaleString() },
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      }
      if (args.item.action) embed.fields.push({ name: 'Action', value: `Binds \`${args.item.action.type}\` to \`${args.item.action.any}\`` });
      return embed;
    },
    complete: (user, item, guild) => {
      let embed = {
        title: `Completed Purchase`,
        description: `You've successfully purchased Item #${item.id} for $${item.price.toLocaleString()}`,
        thumbnail: user.displayAvatarURL()
      };
      return embed;
    },
    cancel: `**Item Purchase Cancelled** => Successfully cancelled purchase of this item`
  },
  delete: {
    cancel: `**Arctic Store Item Deletion**\n\nSuccessfully cancelled item deletion.`,
    success: `**Arctic Store Item Deletion**\n\nThis item has been successfully removed from the store.`,
    confirmation: (Client, args, item) => {
      let embed = {
        title: `Arctic Store Deletion`,
        description: `Please look over the following information then reply with \`confirm\` to delete the store Item.\nSay \`cancel\` to cancel.`,
        fields: [
          { name: `Description`, value: item.description, inline: true },
          { name: `Name`, value: item.name },
          { name: `Quantity`, value: item.quantity.toLocaleString() },
          { name: `Price`, value: `$${item.price.toLocaleString()}` },
          { name: `Times Purchased`, value: item.timesPurchased.toLocaleString() }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      if (args.item.action) embed.fields.push({ name: 'Action', value: `Binds \`${args.item.action.type}\` to \`${args.item.action.any}\`` });
      return embed;
    }
  },
  list: {
    global: (Client, list) => {
      let embed = {
        title: `Arctic Studios Store`,
        description: `All items are purchased with coins earned by being active, leveling up, or playing our games.\nUse \`-store [itemID]\` to view more information for an item.`,
        fields: []
      };
      return embed;
    },
    individual: (Client, item) => {
      let embed = {
        title: `Store Item #${item._id}`,
        description: `Information for \`Item #${item._id}\`. If you would like to purchase this item execute \`-buy ${item._id}\``,
        fields: [
          { name: `Name`, value: item.name },
          { name: `Description`, value: item.description, inline: true },
          { name: `Price`, value: `$${item.price.toLocaleString()}` },
          { name: `Quantity`, value: item.quantity.toLocaleString() },
          { name: `Times Purchased`, value: item.timesPurchased.toLocaleString() },
          { name: `Last Purchased`, value: `${item.lastPurchased ? item.lastPurchased : 'Not purchased yet'}` }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    }
  },
}

exports.inventory = {
  errors: {
    noInventory: `**Arctic Studios User Inventory** => It appears you do not have an inventory, try running the command again.`,
    empty: `**Arctic Studios User Inventory** => This user has no items in their inventory.`,
    invalidUse: `**Arctic Studios User Inventory => To use an advertisement you send a message in #advertisements`
  },
  use: {
    noneLeft: `**Arctic Studios User Inventory** => You do not have any of this item left. It has been removed from your inventory.`,
    cancel: `**Arctic Studios User Inventory** => Successfully cancelled item usage.`,
    noAdverts: `**Arctic Studios User Inventory** => You do not have any Advertisements. You can purchase Advertisements from the shop.`,
    confirmation: (user, args) => {
      let embed = {
        title: `Using inventory item #${args.userItem.id}`,
        description: `Are you sure you'd like to use this item? Using this will either deduct your quantity or remove it from your inventory if you have none left.\nSay \`confirm\` if you wish to do so.\nSay \`cancel\` to cancel.`,
        fields: [
          { name: `Name`, value: args.userItem.name || 'unknown' },
          { name: `Quantity`, value: args.userItem.quantity.toLocaleString() },
        ]
      };
      if (args.action) embed.fields.push({ name: 'Action', value: args.action });
      return embed;
    },
    complete: (user, item) => {
      let embed = {
        title: `Item Used`,
        description: `You've successfully used Item #${item.id}!`,
        thumbnail: user.displayAvatarURL()
      };
      return embed
    }
  },
  delete: {
    cancel: `**Inventory Item Deletion**\n\nSuccessfully cancelled item deletion.`,
    success: `**Inventory Item Deletion**\n\nThis item has been successfully removed from the user.`,
    confirmation: (user, item) => {
      let embed = {
        title: `Inventory Item Deletion`,
        description: `Please look over the following information then reply with \`confirm\` to delete the user's item.\nSay \`cancel\` to cancel.`,
        fields: [
          { name: `Name`, value: item.name || 'unknown' },
          { name: `Quantity`, value: item.quantity.toLocaleString() },
        ]
      };
      return embed;
    },
    complete: (user, item) => {
      return {
        title: `Item Deleted`,
        description: `You've successfully deleted Item #${item.id} from ${user.username}'s inventory`,
        thumbnail: user.displayAvatarURL()
      }
    }
  },
  inventory: (user, data) => {
    return {
      title: `Inventory for ${user.username}`,
      description: `This is a list of all items this user owns. Use \`-store [itemId]\` to see the item's information.`,
      fields: [],
      thumbnail: user.user ? user.user.displayAvatarURL() : user.displayAvatarURL()
    }
  }
}

exports.payments = {
  errors: {
    failedDelete: `**Payment Deletion** => This invoice was unable to be deleted. Try again.`,
    invalidChannel: `**Invalid Command Usage** => That command can only be executed in a \`ticket\` channel.`,
    invalidPermissions: `**Invalid Permissions** => You require the \`Human Resources\` role or higher to execute that command.`,
    cancel: `**Exiting Payments Process** => Cancelling the payments system.`,
    invalidCommissionID: `**Invalid Syntax** => The commission ID provided was not found in our systems. Exiting the payments system.`,
    invalidEmail: `**Invalid Syntax** => The Email address provided is invalid. Exiting the payments system.`,
    invalidPrice: `**Invalid Syntax** => The amount you entered is invalid. Exiting the payment system.`,
    noUserTag: `**Invalid Syntax** => You must @tag the user. Exiting the payment system.`,
    noFreelancerTag: `**Invalid Syntax** => You must @tag the freelancer. Exiting the payment system.`,
    invalidFreelancerTag: `**Invalid Syntax** => The member that you tagged is not an Arctic Studios Freelancer. Exiting the payment system.`,
    invalidFindChannel: `**Invalid Permissions** => Due to your permissions. That command must be executed in a \`ticket\` channel.`,
    noInvoicesFound: `**Invalid Syntax** => No invoices were found in this channel. Please create one with \`-payment create\` first.`
  },
  delete: {
    cancel: `**Arctic Invoice Deletion**\n\nSuccessfully cancelled invoice deletion.`,
    success: `**Arctic Invoice Deletion**\n\nThe invoice has successfully been deleted.`,
    confirmation: (Client, invoice) => {
      let embed = {
        title: `Arctic Invoice Deletion => ${invoice.invoiceID}`,
        description: `Please read over the following information and reply with \`confirm\` to delete the invoice.`,
        fields: [
          { name: `Note`, value: invoice.invoiceInfo.note, inline: true },
          { name: `Commission ID`, value: invoice.invoiceExtras.commissionID },
          { name: `Client`, value: invoice.clientInfo.clientUserName },
          { name: `Total Cost`, value: `$${invoice.invoiceInfo.totalPrice.toLocaleString()}` },
          { name: `Client Email Address`, value: invoice.clientInfo.clientEmail },
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    }
  },
  create: {
    enterCustomID: `**Arctic Payment Creation**\n \nWelcome to Arctic Studios Invoice Creation system. In this dialogue, we will ask questions about the commission and the client so that we can automatically send an invoice to the right place.\n \nWant to Quit? Just type \`cancel\` at any time or wait 100 seconds.\n \nUnfortunately we couldn't find any commissions in this channel, Please enter a commissionID.`,
    confirmCommission: (Client, commission) => {
      let embed = {
        title: `Arctic Payment Creation`,
        description: `Please read over the following information and check that it is correct before proceeding with the selected commission. If it is correct, please reply with \`confirm\` to continue.`,
        fields: [
          { name: `Description`, value: `${commission.commissionInfo.description}`, inline: true },
          { name: `Budget`, value: `${commission.commissionInfo.budget}` },
          { name: `Timeframe`, value: `${commission.commissionInfo.timeframe}` },
          { name: `Client`, value: `${commission.commissionClient.clientUserName}` }
        ]
      };
      return embed;
    },
    multiChoice: (Client, count) => {
      let embed = {
        title: `Arctic Payment Creation`,
        description: `We found ${count} commissions in this channel. Please reply with the number of the commission you want within 10 seconds.`,
        fields: []
      }
      return embed;
    },
    client: `**Arcitc Payment Creation**\n \nPlease enter the email address that the invoice will be sent to.`,
    agreedPrice: `**Arctic Payment Creation**\n \nPlease enter the agreed price for this invoice.`,
    note: `**Arctic Payment Creation**\n \nPlease enter a note that will accompany the invoice. Include as much detail about the commission as possible.`,
    clientTag: `**Arcitc Payment Creation**\n \nPlease now @mention the client.`,
    freelancerTag: `**Arctic Payment Creation**\n \nPlease now @mention the freelancer.`,
    confirmation: (Client, payment) => {
      let embed = {
        title: `Arctic Payment Creation => ${payment.commissionID}`,
        description: `Please look over the following information and check that it is correct. If everything is right, reply with \`confirm\` to send the invoice.`,
        fields: [
          { name: `Note`, value: payment.note, inline: true },
          { name: `Client`, value: payment.clientUserName },
          { name: `Freelancer`, value: payment.freelancerName },
          { name: `Total Cost`, value: `$${payment.totalPrice.toLocaleString()}` },
          { name: `Client Email Address`, value: payment.clientEmail },
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    },
    complete: (Client, args) => {
      let embed = {
        title: `Arctic Studios Invoice => ${args.storeInvoice.arcticID}`,
        description: `The invoice has been sent and can be paid [here](https://www.paypal.com/invoice/p#${args.invoice.id} 'View your Invoice here').`,
        fields: [
          { name: `Note`, value: args.payment.note, inline: true },
          { name: `Client`, value: args.payment.clientUserName },
          { name: `Freelancer`, value: args.payment.freelancerName },
          { name: `Total Cost`, value: `$${args.payment.totalPrice.toLocaleString()}` },
          { name: `Client Email Address`, value: args.payment.clientEmail }
        ],
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    },
    sending: (stage) => {
      return `**Arctic Payment Creation**\n \n${stage}`
    }
  },
  find: {
    invoice: (Client, invoice) => {
      let embed = {
        title: `Arctic Studios Invoice => ${invoice.arcticID} => ${invoice.invoiceExtras.invoiceStatus}`,
        description: `Here is some details about invoice ${invoice.invoiceID}. You can view the invoice [here](https://paypal.com/p#${invoice.invoiceID} 'View your Invoice here').`,
        fields: [
          { name: `Note`, value: invoice.invoiceInfo.note, inline: true },
          { name: `Commission ID`, value: invoice.invoiceExtras.commissionID },
          { name: `Client`, value: invoice.clientInfo.clientUserName },
          { name: `Total Cost`, value: `$${invoice.invoiceInfo.totalPrice.toLocaleString()}` },
          { name: `Client Email Address`, value: invoice.clientInfo.clientEmail },
        ],
        footer: {
          text: invoice.invoiceExtras.invoiceSentAt
        },
        thumbnail: Client.client.user.displayAvatarURL()
      };
      return embed;
    },
    multi: (Client, count) => {
      let embed = {
        title: `Arctic Studios Invoices`,
        description: `We found ${count} invoices in this channel. Please reply with the number of the invoice you would like to select.`,
        fields: []
      };
      return embed;
    }
  }
}

exports.admin = {
  errors: {
    invalidPermissions: `**Invalid Permissions** => You must have the \`Human Resources\` role or Higher to execute that command.`
  },
  give: {
    intro: `**Arctic Studios Admin**\n \nTo give coins to a user, execute the command with their name and an amount to give. \`-admin give <username> <amount>\`.`,
    success: (Client, args, user) => {
      let embed = {
        title: `Arctic Studios Admin`,
        description: `Successfully completed transaction.`,
        fields: [
          { name: `From`, value: `\`Arctic Bank\`` },
          { name: `To`, value: `\`${user.user.tag}\`` },
          { name: `Amount`, value: `\`$${args.stake.toLocaleString()}\`` }
        ],
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      };
      return embed;
    }
  },
  admin: {
    intro: `**Arctic Studios Admin**\n \nTo remove coins from a user, execute the command with their name and an amount to remove. \`-admin remove <username> <amount>\`.`,
    success: (Client, args, user) => {
      let embed = {
        title: `Arctic Studios Admin`,
        description: `Successfully completed transaction.`,
        fields: [
          { name: `From`, value: `\`${user.user.tag}\`` },
          { name: `To`, value: `\`Arctic Bank\`` },
          { name: `Amount`, value: `\`$${args.stake.toLocaleString()}\`` }
        ],
        footer: {
          text: Client.utils.Logger._timestamp()
        }
      };
      return embed;
    }
  }
}

exports.payouts = {
  errors: {
    invalidPermissions: `**Invalid Permissions** => You must have the \`Arctic Team\` role or Higher to execute that command.`,
    invalidChannel: `**Invalid Channel** => That command can only be executed in a \`ticket\` channel.`,
    cancel: `**Exiting Payouts System** => Canceled the Payouts System.`
  },
  create: {
    enterCustomID: `**Arctic Studios Payouts**\n \nUnfortunately, we couldn't find any invoices in this channel. Please create an invoice before requesting a Payout.`,
    confirmation: (Client, invoice) => {
      let embed = {
        title: `Arctic Studios Payouts`,
        description: `Please read over the following information and reply with \`confirm\` to start creating a payout for the invoice.`,
        fields: [
          { name: `Note`, value: invoice.invoiceInfo.note, inline: true },
          { name: `Commission ID`, value: invoice.invoiceExtras.commissionID },
          { name: `Client`, value: invoice.clientInfo.clientUserName },
          { name: `Total Cost`, value: `$${invoice.invoiceInfo.totalPrice.toLocaleString()}` },
          { name: `Client Email Address`, value: invoice.clientInfo.clientEmail },
        ]
      };
      return embed;
    },
    multiChoice: (Client, length) => {
      let embed = {
        title: `Arctic Studios Payouts`,
        description: `We found a total of ${length} invoices in this channel. Please read over the details and reply with the number of the invoice you would like to create a payout for.`,
        fields: []
      };
      return embed;
    }
  }
}

exports.advent = {
  errors: {
    invalidPermissions: `**Invalid Permissions** => You require the \`Board of Directors\` role or Higher to execute that command.`,
    invalidParam: `**Invalid Paramater** => The only syntax for that command is \`start\`.`,
    invalidDate: `**Invalid Start Point** => Advent can only start on the first of December.`,
    alreadyClaimed: `**Already Claimed** => You have already claimed that days reward. Please wait for a new day.`
  },
  post: (Client, adventMsg) => {
    let embed = {
      title: `🎄 Arctic Advent Calender 🎄`,
      description: adventMsg,
      thumbnail: Client.client.user.displayAvatarURL()
    };
    return embed;
  },
  claim: (Client, user, reward, day) => {
    let embed = {
      title: `🎄 Congratulations ${user.username}`,
      description: `You got \`${reward.name}\`.\n \nYou claimed for \`${day.date}\``,
      thumbnail: user.displayAvatarURL()
    }
    return embed;
  },
  items: {
    fivePercent: `**Congratulations You Won** => You won an extra special prize of **5% off any Arctic Studios Order**. Screenshot this and open a ticket in Arctic to claim.\n \nQuote the code \`ADVENT5\``,
    tenPercent: `**Congratulations You Won** => You won an extra special prize of **10% off any Arctic Studios Order**. Screenshot this and open a ticket in Arctic to claim.\n \nQuote the code \`ADVENT10\``,
    dectom: `**Congratulations You Won** => You won an extra special prize of **Free Discord bot from Dectom**. Screenshot this and open a ticket in Arctic to claim.\n \nQuote the code \`FR33D3CT0M\``,
    austin: `**Congratulations You Won** => You won an extra special prize of **Free Plugin from Austin**. Screenshot this and open a ticket in Arctic to claim.\n \nQuote the code \`FR33AU3T1N\``,
    jordan: `**Congratulations You Won** => You won an extra special prize of **Free Website / Theme from Jordan**. Screenshot this and open a ticket in Arctic to claim.\n \nQuote the code \`FR33J0RDAN\``,
    redeye: `**Congratulations You Won** => You won an extra special prize of **Free Logo from RedEye**. Screenshot this and open a ticket in Arctic to claim.\n \nQuote the code \`FR33R3D3Y3\``
  },
  rewards: (Client, user) => {
    let embed = {
      title: `${user.username}'s Advent Rewards`,
      description: `You have no current rewards. To claim some, please react to the messages in **#advent-calendar**.`,
      thumbnail: user.displayAvatarURL()
    };
    return embed;
  }
}