<h1 style="text-align: center">Arctic Studios Discord Messages Fixes</h1>

<p>First off, what is this and why does it exist? Well then, let's take you through a tale as old as time, the story of Dectom's poor grammar and spelling. </p>

Things have gotten so bad, that this repository exists just for one file, the file that contains all messages included in the Discord bot. Feel free to make merge requests to fix grammar and spelling issues at any time.